var chart1 = AmCharts.makeChart("chartdiv1", {
    "legend": {
        "useGraphSettings": true
    },
    "theme": "light",
    "type": "serial",
    "dataProvider": [{
        "periode": "Januari",
        "in": 350,
        "out": 420
    }, {
        "periode": "Februari",
        "in": 170,
        "out": 30
    }, {
        "periode": "Maret",
        "in": 100,
        "out": 200
    }, {
        "periode": "April",
        "in": 300,
        "out": 230
    }, {
        "periode": "Mei",
        "in": 340,
        "out": 210
    }, {
        "periode": "Juni",
        "in": 260,
        "out": 440
    }, {
        "periode": "Juli",
        "in": 640,
        "out": 720
    }, {
        "periode": "Agustus",
        "in": 850,
        "out": 900
    }, {
        "periode": "September",
        "in": 900,
        "out": 1000
    }, {
        "periode": "Oktober",
        "in": 322,
        "out": 123
    }, {
        "periode": "November",
        "in": 653,
        "out": 321
    }, {
        "periode": "Desember",
        "in": 454,
        "out": 344
    }],
    "valueAxes": [{
        "position": "left",
        "title": "Total (dalam ribuan rupiah)"
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "Total Pemasukan: <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "Pemasukan",
        "type": "column",
        "valueField": "in",
        "fillColors":"#379e51"

    }, {
        "balloonText": "Total Pengeluaran: <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "Pengeluaran",
        "type": "column",
        "valueField": "out",
        "fillColors":"#c3100c"
    }],
    "plotAreaFillAlphas": 0.1,
    "categoryField": "periode",
    "categoryAxis": {
        "gridPosition": "start"
    }
});

var chart2 = AmCharts.makeChart("chartdiv2", {
    "legend": {
        "useGraphSettings": true
    },
    "theme": "light",
    "type": "serial",
    "dataProvider": [{
        "periode": 2010,
        "in": 1350,
        "out": 2420
    }, {
        "periode": 2011,
        "in": 2170,
        "out": 1230
    }, {
        "periode": 2012,
        "in": 1100,
        "out": 2200
    }, {
        "periode": 2013,
        "in": 2300,
        "out": 2230
    }, {
        "periode": 2014,
        "in": 3340,
        "out": 1210
    }, {
        "periode": 2015,
        "in": 1260,
        "out": 3440
    }, {
        "periode": 2016,
        "in": 2640,
        "out": 2720
    }],
    "valueAxes": [{
        "position": "left",
        "title": "Total (dalam ribuan rupiah)"
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "Total Pemasukan: <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "Pemasukan",
        "type": "column",
        "valueField": "in",
        "fillColors":"#379e51"

    }, {
        "balloonText": "Total Pengeluaran: <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "Pengeluaran",
        "type": "column",
        "valueField": "out",
        "fillColors":"#c3100c"
    }],
    "plotAreaFillAlphas": 0.1,
    "categoryField": "periode",
    "categoryAxis": {
        "gridPosition": "start"
    }
});